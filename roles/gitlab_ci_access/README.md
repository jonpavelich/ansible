# Summary
This role enables server access for GitLab CI runners. It is typically
used in conjuction with the [container_host](../container_host/README.md) role
to allow GitLab CI to connect to the server to update and restart
containers.

The contents of the `gitlab_ci_authorized_keys` variable will be written
to `~/.ssh/authorized_keys` for the `gitlab` user.

## Example
In a playbook:
```yaml
vars:
  gitlab_ci_authorized_keys: |
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJfn5ir47BuCEa9quRCKMLoVWv6HgWxlkjEhFNfjucat gitlab
tasks:
  - include_role: gitlab_ci_access
```
