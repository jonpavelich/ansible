# Summary
This role configures a CentOS Stream host to run OCI or Docker containers with Podman,
managed by systemd. The containers will be run as systemd user services
under the `podman` user. Caddy is used as a reverse proxy to expose any
services with the `host_url` variable defined, and will automatically
obtain TLS certificates when possible. If `host_url` is not defined then
the container will not be exposed externally (e.g. database containers).

The host's IP address is added to the container as `container-host`.
For example, if you expose redis on `6379` like in the example below,
then you can access it from another container at `container-host:6379`.

# Variables
| Key                                 | Summary                                                    |
| ----------------------------------- | ---------------------------------------------------------- |
| container_host_services             | services to configure, see below                           |
| container_host_remove_services      | previously configured services to remove                   |
| container_host_firewall_ports       | extra ports to open on the firewall (e.g. `5432/tcp`)      |
| container_host_firewall_services    | extra services to open on the firewall (e.g. `postgresql`) |
| container_host_registry_auth | auth credentials for private container registries          |

## Services
Services to run are configured via the `container_host_services` variable. Each
item in this variable defines a separate container to manage. The properties
are as follows:

| Key            | Summary                                                              |
| -------------- | -------------------------------------------------------------------- |
| name           | name to give the container (alphanumeric and hyphens - no spaces)    |
| image          | image to pull (a tag can be optionally included)                     |
| container_port | the port in the container to expose                                  |
| host_port      | the port on the host to map to container_port                        |
| env            | (optional, multi-line) environment variables to set in the container |
| host_url       | (optional) the FQDN to use for reverse proxying                      |

If you want to remove an existing service from a host, simply move the
definition from `container_host_services` to `container_host_remove_services`
and run the playbook. This will remove the service (with the exception
that the Docker image will not be deleted from podman's image cache.
Once this is done, you can remove the service from the playbook.

## Registries
Private registries are configured via the `container_host_registry_auth` variable.
Each item in this variable defines a separate registry. The properties are as follows:

| Key      | Summary                   |
| -------- | ------------------------- |
| registry | private registry URL      |
| username | username for the registry |
| password | password for the registry |

## Example
```yaml
---
vars:
  container_host_registry_auth:
    # Credentials for a private container registry
    - registry: registry.gitlab.com
      username: jonpavelich
      password: wow-so-secure

  container_host_services:
    # An internal container
    - name: redis
      image: docker.io/library/redis
      container_port: 6379
      host_port: 6379

    # An external container (exposed at demo1.example.com:443)
    - name: nginx-demo-1
      image: docker.io/nginxdemos/hello
      container_port: 80
      host_port: 8001
      host_url: demo1.example.com

    # Another external container with envvars set (exposed at demo2.example.com:443)
    - name: nginx-demo-2
      image: docker.io/nginxdemos/hello
      container_port: 80
      host_port: 8002
      host_url: demo2.example.com
      env: |
        VAR_1=some value
        VAR_2=another value

...
```

# Scripts
## Container Updates
Update scripts called `container-update-<name>` are generated and placed in
`/usr/local/bin`. When run, they will pull any available image updates then
restart the container.

## Container Logs
Log scripts called `container-logs-<name>` are generated and placed in
`/usr/local/bin`. When run, they will call the appropriate `journalctl`
command to get logs from the container.

You can optionally pass a format string which will be given to `journalctl`. All of the
[journalctl output formats](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs#output-formats)
are recognized, as well as `raw` which is an alias for `cat`.

```bash
# Default format for container "example"
$ container-logs-example

# Just print the messages (no timestamps or metadata) for container "example"
$ container-logs-example raw
```

## Status, Start, Stop
Containers run as a systemd user services, so you can manage them using `systemctl`.

1. To access `podman`'s user services you must cecome the `podman` user (e.g. `sudo su - podman`).
1. Use the standard `systemctl` commands with the `--user` flag:

    ```bash
    # Check status
    systemctl --user status container-<name>
    
    # Stop the container
    systemctl --user stop container-<name>

    # Start a stopped container
    systemctl --user start container-<name>
    ```