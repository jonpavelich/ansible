# Summary
This role configures automated backups to [Backblaze B2](https://www.backblaze.com/b2) using [Restic](https://restic.net).
 
# Variables
| Key                        | Summary                                                     |
| -------------------------- | ----------------------------------------------------------- |
| backup_b2_account_id       | Backblaze B2 account ID (or application ID)                 |
| backup_b2_account_key      | Backblaze B2 account key (or application key)               |
| backup_b2_bucket           | Backblaze B2 bucket name                                    |
| backup_targets             | Single-line space-separated list of directories to back up  |
| backup_exclude             | Multi-line list of patterns to exclude from backups         |