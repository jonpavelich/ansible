# Automatic LUKS Unlock on CentOS 8
Tested on: CentOS 8 Stream

## Summary
This describes how to automatically unlock a system LUKS drive on boot if a keyfile device is present, falling back to a passphrase otherwise.
The `rd.luks.key` boot parameter is ignored ([RH Bug 905683](https://bugzilla.redhat.com/show_bug.cgi?id=905683)), leading to behaviour where either a keyfile is used (with no passphrase fallback) OR a passphrase is used (with no keyfile used), but not both.

These modifications serve to auto-mount and use a keyfile device if present, and gracefully fallback to passphrase prompting if the keyfile is missing or invalid.

Source: [User prudy on Fedora Forums](https://forums.fedoraforum.org/showthread.php?241942-Unlocking-LUKS-with-USB-key-method-seeking-help-to-improve&p=1797174#post1797174)

## Prerequisites
1. CentOS installed using encrypted LVM:
    1. `/boot` must be unencrypted
    1. All LVM volumes must be on a single LUKS partition (`/dev/sda3` in this example)

1. A directory at `/mnt/unlocker`

1. A key device. It can be a USB or SD card, but either way must be a single partition formatted as VFAT.
    ```
    fdisk /dev/sdx
    mkfs.fat -F32 /dev/sdx1
    mount /dev/sdx1 /mnt/unlocker
    ```

1. A keyfile stored on the root of your key device (named `unlocker.key`) and added to your LUKS volume.
    ```
    dd if=/dev/urandom of=/mnt/unlocker/unlocker.key bs=1024 count=4
    cryptsetup luksAddKey /dev/sda3 /mnt/unlocker/unlocker.key
    ```

## Modifications
Note that in these files you must replace `1111-1111` and `22222222-2222-2222-2222-222222222222` with your UUIDs.
```
/dev/sda3: UUID="22222222-2222-2222-2222-222222222222" TYPE="crypto_LUKS"
/dev/sdx1: UUID="1111-1111" TYPE="vfat"
```

1. Edit `/etc/crypttab` to add a keyfile of `/tmp/unlocker.key`:
```
luks-22222222-2222-2222-2222-222222222222 UUID=22222222-2222-2222-2222-222222222222 /tmp/unlocker.key
```

1. Edit `/etc/fstab` to add a line for your keyfile device
```
UUID=1111-1111 /mnt/unlocker vfat ro,umask=0277,x-systemd.device-timeout=5,noauto 0 0
```

1. Create a new file at `/etc/dracut.conf.d/luks-workaround.conf`:
```
# Use fstab for discovering mount options
use_fstab="yes"

# Define initramfs fstab entry for luks key device.
# A timeout defines time after which fallback to password request is used if the device is not present/inserted
# This line is actually a copy of the same entry from /etc/fstab
fstab_lines+=("UUID=1111-1111 /mnt/unlocker vfat ro,umask=0277,x-systemd.device-timeout=5,noauto 0 0")

# Include workaround for systemd to fallback to passphrase request for resume partition if no luks key device is present
install_items+=" /etc/systemd/system/systemd-cryptsetup@.service.d/luks-key-device.conf"

# Include workaround for systemd to mount luks key device again in OS, which was just mounted in initramfs
install_items+=" /etc/systemd/system/mnt-unlocker.mount.d/umount-initramfs.conf"
```

1. Create a new file at `/etc/systemd/system/mnt-unlocker.mount.d/umount-initramfs.conf`:
```
# Starting with Fedora 21, when a device is mounted in initramfs,
# its .mount systemd unit is set to inactive when leaving initramfs,
# but it cannot be started again immediately after that in OS.
# This file makes that possible.

[Unit]
Conflicts=initrd-cleanup.service
```

1. Create a new file at `/etc/systemd/system/systemd-cryptsetup@.service.d/luks-key-device.conf`:
```
# This is a workaround for systemd not being able to ask for a luks password
# if the device with luks key file is not available (e.g. USB not inserted).
# Bugzilla 905683 "rd.luks.key is ignored"

[Unit]
Description=Bugzilla 905683 Cryptography Setup for %I
Documentation=https://bugzilla.redhat.com/show_bug.cgi?id=905683 http://forums.fedoraforum.org/showpost.php?p=1696031&postcount=45
After=mnt-unlocker.mount
Wants=mnt-unlocker.mount
RequiresMountsFor=/tmp/unlocker.key

[Service]
ExecStartPre=-/usr/bin/ln -s /mnt/unlocker/unlocker.key /tmp/unlocker.key
```

1. Ensure your boot configuration does **NOT** contain any `luks.*` parameters. If you changed it, rebuild grub:
```
grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
```

1. Rebuild initramfs:
```
dracut -f
```

1. Reboot :)
