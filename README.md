# Ansible
This is a collection of Ansible Playbooks and Roles for managing my 
infrastructure. Roles are used to make these scripts as re-useable
and composable as possible.

**Warning:** These files are designed to fit my specific needs and may not work as expected outside of the environment they were designed for. **Please read them carefully before attempting to use them!**

## Roles
Each role has its own README which describes its purpose and usage in detail.

- [backup](./roles/backup/README.md): set up backups using [Restic](https://restic.net)
- [bootstrap](./roles/bootstrap/README.md): common setup for all systems
- [container_host](./roles/container_host/README.md): run OCI or Docker containers via Podman and Systemd
- [gitlab_ci_access](./roles/gitlab_ci_access/README.md): provision SSH access for GitLab CI runners
- [postgresql](./roles/postgresql/README.md): create PostgreSQL users and databases
- [update](./roles/update/README.md): update a system
- [zfs](./roles/zfs/README.md): install ZFS on Linux

## Playbooks

- [bootstrap](./bootstrap.yml): prepares a brand new server to run other playbooks
- [home_server](./home_server.yml): provisions my home server
- [snake](./battlesnake.yml): provisions my [Battlesnake](https://play.battlesnake.com/) server
